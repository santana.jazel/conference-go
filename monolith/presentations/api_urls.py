from django.urls import path

from .api_views import (
    api_list_presentations,
    api_show_presentation,
    api_approve_presentation,
    api_rejection_presentation,
)


urlpatterns = [
    path(
        "conferences/<int:conference_id>/presentations/",
        api_list_presentations,
        name="api_list_presentations",
    ),
    path(
        "presentations/<int:id>/",
        api_show_presentation,
        name="api_show_presentation",
    ),
    path(
        "presentations/<int:id>/approval/",
        api_approve_presentation,
        name="api_approve_presentations",
    ),
    path(
        "presentations/<int:id>/rejection/",
        api_rejection_presentation,
        name="api_reject_presentations",
    ),
]
