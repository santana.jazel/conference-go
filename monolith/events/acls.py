import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json

# from .models import Location


def get_picture(city, state):
    # response = requests.get(
    #     f"https://api.pexels.com/v1/search?query={city}%20{state}",
    #     headers=headers,
    # )
    # picture = json.loads(response.content) OR

    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    query = {"query": f"{city}, {state}"}
    response = requests.get(url, params=query, headers=headers)

    # content = json.loads(response.content)### or you can use the below
    content = response.json()

    try:
        picture_url = content["photos"][0]["src"]["original"]
        return {"picture_url": picture_url}
    except {KeyError, IndexError}:
        return {"picture_url": None}


def get_weather_data(city, state):
    lat_lon_url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {"q": f"{city}, {state}", "appid": OPEN_WEATHER_API_KEY}
    response = requests.get(lat_lon_url, params=params)
    content = response.json()
    if content == []:
        lat = 0
        lon = 0
    else:
        lat = content[0]["lat"]
        lon = content[0]["lon"]

    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat,
        "lon": lon,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY,
    }

    response = requests.get(url, params=params)
    content = response.json()
    description = content["weather"][0]["description"]
    temp = content["main"]["temp"]

    return {
        "description": description,
        "temp": temp,
    }
