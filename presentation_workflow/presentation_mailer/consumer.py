import json
import pika  # type: ignore
from pika.exceptions import AMQPConnectionError  # type:ignore
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")

django.setup()

# Setup connection
# try:
parameters = pika.ConnectionParameters(host="rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
# except AMQPConnectionError:
#     print("Did not work")
#     time.sleep(2.0)


def process_approval(ch, method, properties, body):
    message = json.loads(body)
    name = message.get("presenter_name")
    email = message.get("presenter_email")
    title = message.get("title")

    subject = "Your presentation has been accepted"
    body = f"{name}, we're happy to tell you that your presentation {title} has been accepted"
    from_email = "admin@conference.go"
    to_email = [email]

    try:
        send_mail(subject, body, from_email, to_email, fail_silently=False)
    except Exception as e:
        print(f"Failed to send email: {str(e)}")


#  Setup the approval queue
channel.queue_declare(queue="presentation_approvals")
channel.basic_consume(
    queue="presentation_approvals",
    on_message_callback=process_approval,
    auto_ack=True,
)


def process_rejection(ch, method, properties, body):
    message = json.loads(body)
    print(message)
    name = message.get("presenter_name")
    email = message.get("presenter_email")
    title = message.get("title")

    subject = "Your presentation has been rejected"
    body = f"{name}, we're sorry to inform you that your presentation {title} has been rejected"
    from_email = "admin@conference.go"
    to_email = [email]

    try:
        send_mail(subject, body, from_email, to_email, fail_silently=False)
    except Exception as e:
        print(f"Failed to send email: {str(e)}")


# Setup rejection queue
channel.queue_declare(queue="presentation_rejections")
channel.basic_consume(
    queue="presentation_rejections",
    on_message_callback=process_rejection,
    auto_ack=True,
)

channel.start_consuming()
